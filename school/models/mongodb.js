const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/school', {
    useNewUrlParser:true,
    useUnifiedTopology:true
})
.then(() => console.log('Connected to shop DB succesfully'))
.catch(err => console.log('failed to connect to shop DB',err))


require('./school.model')
require('./student.model')
require('./teacher.model')
