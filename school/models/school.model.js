const mongoose = require('mongoose')
const Joi = require('joi')
const jwt = require('jsonwebtoken')
const config = require('config')

const schoolSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
    },
    email:{
        type:String,
        require:true,
        unique:true

    },
    sector:{
        type:String,
        required:true
    },
    district:{
        type:String,
        required:true,
    }
})

 const School = mongoose.model('School',schoolSchema)

 function validateSchool(school){
    const schema = {
        name:Joi.string().max(255).min(3).required(),
        email: Joi.string().max(255).min(3).required().email(),
        district:Joi.string().max(255).min(3).required(),
        sector:Joi.string().max(255).min(3).required()
    }

    return Joi.validate(school,schema)
 }

 module.exports.School = School
 module.exports.validateSchool = validateSchool
