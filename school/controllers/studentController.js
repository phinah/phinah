const express = require('express')
const _ = require('lodash')
const {Student,validateStudent} = require('../models/student.model')
let router = express.Router()
const {School}=require('../models/school.model')
const authMidleware = require('../middleware/auth')
const admin = require('../middleware/admin')


router.get('/', async (req,res) => {
    const student = await Student.find().sort({name:1})

    return res.send(student)
})

router.get('/school/:id', authMidleware, async (req,res) => {
    const student = await Student.find({schoolId:req.params.id})
    return res.send(student).status(200)
})


router.get('/sector/:name', authMidleware, async (req,res) => {
    const school = await School.find({sector:req.params.name})
    // return res.send(school).status(200)
    let num = 0;
    for(let i = 0; i<school.length;i++){
        const student = await Student.find({schoolId:school[i]._id}).countDocuments()

        num +=student;
    }
    
    res.send(num.toString())
})


router.get('/district/:name', authMidleware, async (req,res) => {
    const school = await School.find({district:req.params.name})
    // return res.send(school).status(200)
    let num = 0;
    for(let i = 0; i<school.length;i++){
        const student = await Student.find({schoolId:school[i]._id}).countDocuments()

        num +=student;
    }
    console.log(num)
})




router.get('/:_id', authMidleware, async (req,res) => {
    const student = await Student.find({_id:req.params._id})

    return res.send(student).status(200)
})

router.post('/',[authMidleware,admin], async (req,res) => {
    const {error} = validateStudent(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    let student = await Student.findOne({name:req.body.name,gender:req.body.gender,age:req.body.age,schoolId:req.body.schoolId})
    if(student) return res.send('student already there').status(400)

    school = await School.findOne({_id:req.body.schoolId})
    if(!school) return res.send(`We dont have such school with ${req.body.schoolId} in our system`).status(404)

    student = new Student(_.pick(req.body,['name','gender', 'age','schoolId']))
    await student.save()
    return res.send(_.pick(student,['_id','name','gender', 'age','schoolId']))
})

router.put('/', [authMidleware,admin], async (req,res) => {
    const {error} = validateStudent(req.body)
    if(error) return res.send(error.details[0].message).status(400)



    school = await School.find({_id:req.body.schoolId})
    if(!school) return res.send(`We dont have such school with ${req.body.schoolId} in our system`).status(404)


    Student.findOneAndUpdate({_id:req.body._id},req.body,{new:true})
    .then(student => res.send(student).status(200))
    .catch( err => res.send(err).status(404))
})

router.delete('/:id', [authMidleware,admin], async (req,res) => {

    Student.findOneAndRemove({id:req.params.id})
    .then(student => res.send(student).status(200))
    .catch( err => res.send(err).status(400))
})

module.exports = router;