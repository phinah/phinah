const hashPassword = require('../utils/hash')
const bcrypt = require('bcrypt')
const jwt  =  require('jsonwebtoken')
const Joi = require('joi')
const _= require('lodash')
const config = require('config')
const express = require('express');
const {Teacher} =  require('../models/teacher.model')
var router = express.Router();


router.post('/jwt', async (req,res) => {
    const {error} = validate(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    let teacher  = await Teacher.findOne({email:req.body.email})
    if(!teacher) return res.send('Invalid email or password').status(400)

    const validPassword = await bcrypt.compare(req.body.password,teacher.password)
    if(!validPassword) return res.send('Invalid email or password').status(400)

    return res.send(teacher.generateAuthToken())
})

router.post('/bcrypt',async (req,res) => {
    const {error} = validate(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    let teacher  = await Teacher.findOne({email:req.body.email})
    if(!teacher) return res.send('Invalid email or password').status(400)

    const validPassword = await bcrypt.compare(req.body.password, teacher.password)
    if(!validPassword) return res.send('Invalid Email or password')
    return res.send(_.pick(teacher,['_id','name','email']))
})

function validate(req){
    const schema = {
        email: Joi.string().max(255).min(3).required().email(),
        password:Joi.string().max(255).min(3).required()
    }
    return Joi.validate(req,schema)
}
module.exports = router;
