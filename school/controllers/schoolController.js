const express = require('express')
const _ = require('lodash')
const authMidleware = require('../middleware/auth')
const admin = require('../middleware/admin')

const {School,validateSchool} = require('../models/school.model')
let router = express.Router()

router.get('/', async (req,res) => {
    const school = await School.find().sort({name:1})
    return res.send(school)
})

router.get('/:_id', async (req,res) => {
    const school = await School.find({_id:req.params._id})

    return res.send(school).status(200)
})


router.get('/bysector/:sector', async (req,res) => {
   let scho = await School.find({sector:req.params.sector}).countDocuments()
   
});



router.post('/', [authMidleware,admin], async (req,res) => {
    const {error} = validateSchool(req.body)
    if(error) return res.send(error.details[0].message).status(400)



    let school = await School.findOne({email:req.body.email})
    if(school) return res.send('School already there').status(400)

    school = new School(_.pick(req.body,['name','email', 'sector','district']))
    await school.save()

    
    return res.send(_.pick(school,['_id','name','email', 'sector','district']))
})

router.put('/', [authMidleware,admin], async (req,res) => {
    const {error} = validateSchool(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    School.findOneAndUpdate({_id:req.body._id},req.body,{new:true})
    .then(school => res.send(school).status(200))
    .catch( err => res.send(err).status(404))
})

router.delete('/:id', [authMidleware,admin], async (req,res) => {

    School.findOneAndRemove({id:req.params.id})
    .then(school => res.send(school).status(200))
    .catch( err => res.send(err).status(400))
})

module.exports = router;