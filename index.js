const Joi = require("joi");
const express = require('express');
const app = express();
app.use(express.json())
const port = 8021
let authors = [
    {id: 1 , name: "Ntwari " , email: "ntwaricliberi@gmail.com"},
    {id: 2 , name: "liberi " , email: "liberi@gmail.com"},
    {id: 3 , name: "clarance " , email: "clarance@gmail.com"},
    {id: 4 , name: "rocky " , email: "rocky@gmail.com"},
    {id: 5 , name: "Giti " , email: "giti@gmail.com"},
    {id: 6 , name: "divin " , email: "divin@gmail.com"},
    {id: 7 , name: "daniel " , email: "daniel@gmail.com"},
    {id: 8 , name: "keep it up " , email: "kepp-it-up@gmail.com"}
];


let courses = [{
        id: 1,
        name: "course 1",
        price:200,
        author:1
    },
    {
        id: 2,
        name: "course 2",
        price:200,
        author:3,
    },
    {
        id: 3,
        name: "course 3",
        price:300,
        author:4
    },
    {
        id: 4,
        name: "course 4",
        price:100,
        author:5
    },
]


app.get('/api/authors', (req, res) => {
    return res.send(authors)
});


app.get('/api/authors/:id', (req, res) => {
    const author  = authors.find(c => c.id == req.params.id)

    if(!authors) return res.status(404).send('Author not found')
        return res.send(author);
})
//create 
app.post('/api/authors', (req, res) => {
    const schema = {
        name:Joi.string().min(3).max(30).required(),
        email:Joi.string().email().min(5).max(40).required()
    }
    const result = Joi.validate(req.body,schema)
    console.log(result)
    
    if(result.error){
        return res.status(400).send(result.error.details[0].message);
    }

    let author = {
        id:  authors.length + 1,
        name: req.body.name,
        email: req.body.email
    }
    //append the course to list
    authors.push(author)
    return res.status(201).send(author)
});

//UUPDATE

app.put('/api/authors', (req,res)=>{
    const author = authors.find(c => c.id == req.body.id)
    
    if(!author) return res.status(404).send("Author not fount");

    const schema = {
         id: Joi.number().integer().positive().min(1).required(),
        name:Joi.string().min(3).max(30).required(),
        email:Joi.string().email().min(6).max(30).required()
    }

    const result = Joi.validate(req.body,schema)


    if(!result) return res.status(400).send(result.error.details[0].message)

    author.name = req.body.name
    author.email = req.body.email;

    return res.status(201).send(authors)
})


//DELETE A AUTHOR

app.delete('/api/authors/:id', (req, res) => {
    const author = authors.find(d => d.id == req.params.id)
    
    if(!author) return res.status(404).send("author not Found");

    const index = authors.indexOf(author)
    authors.splice(index,1)
    res.send(authors)
});




//COURSE


app.get('/api/courses', (req, res) => {
    let authorname;

    for ( let item of courses ){
        authorname = authors.find(a => a.id == item.author)
        item.author = authorname.name;
    }



    return res.send(courses)
});


app.get('/api/courses/:id', (req, res) => {

    const course = courses.find(c => c.id == req.params.id)

    if (!course) return res.status(404).send("Course not found")

    return res.send(course)
});

app.post('/api/courses', (req, res) => {

    const schema = {
        name: Joi.string().min(3).max(30).required(),
        price:Joi.number().integer().positive().min(1).required(),
        author: Joi.number().integer().positive().min(1).required()
    }
    const result = Joi.validate(req.body, schema);
    console.log(result)
    if (result.error) {
        return res.status(400).send(result.error.details[0].message);
    }
    let course = {
        id: courses.length + 1,
        name: req.body.name,
        price:req.body.price,
        author:req.body.id
    }
    courses.push(course)
    return res.status(201).send(course)
});

app.put('/api/courses', (req, res) => {

    const course = courses.find(c => c.id == req.body.id)

    if (!course) return res.status(404).send("Course not found")

    const schema = {
        name: Joi.string().min(3).max(30).required(),
        id: Joi.number().required(),
        price: Joi.number().integer().positive().min(1).required(),
        author: Joi.number().integer().positive().min(1).required()
    }
    const result = Joi.validate(req.body, schema);

    if (result.error) {
        return res.status(400).send(result.error.details[0].message);
    }

    course.name = req.body.name
    course.price = req.body.price
    course.author = req.body.author

    return res.status(201).send(course)
});

app.delete('/api/courses/:id', (req, res) => {

    const course = courses.find(c => c.id == req.params.id)

    if (!course) return res.status(404).send("Course not found")

    const index = courses.indexOf(course);

    courses.splice(index, 1);
    res.send(courses)
})
app.listen(port, () => console.log(`Server running on port ${port}`))


// function check(a){
//     for
// }