require('./model/mongodb')
const productController = require('./controllers/productControllers')
const userController = require('./controllers/userControllers')
const authControllers = require('./controllers/auth')
const categoryController = require('./controllers/categoryControllers')
const config = require('config')
const express = require('express') 
const authMidleware = require('./middlewares/auth')

let app = express()
const bodyparser = require('body-parser')

app.use(bodyparser.urlencoded({extended:true}))
app.use(bodyparser.json())

//config
if(!config.get("jwtPrivateKey")){
    console.log("JWT private key is not defined")
    process.exit(1)
}

//welcoming page

app.get('/' ,(req, res) => {
    res.send('Welecome to our Online shop')
})

//set the Controllers path wich will be responding the user
app.use('/api/user',userController)
app.use('/api/auth',authControllers)
app.use('/api/product',productController)
app.use('/api/category',categoryController)


const port = process.env.PORT || 8000
app.listen(port,() => console.log(`listening on port ${port}`))