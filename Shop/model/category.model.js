const mongoose = require('mongoose')
const Joi = require('joi')


const categorySchema = new mongoose.Schema({
    id:{
        type:Number,
        unique:true,
        required:true
    },
    name:{
        type:String,
        required:true,
    }
})

const Category = mongoose.model('Category',categorySchema)

function validateCategory(category){
    const schema = {
        id:Joi.number().integer().max(1000).required(),
        name:Joi.string().min(3).max(300).required()
    }

    return Joi.validate(category,schema)
 }

 module.exports.Category = Category
 module.exports.validateCategory = validateCategory
